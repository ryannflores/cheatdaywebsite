import React, { Component } from 'react';
import GalleryComponent from './pages/Gallery-Component';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="gallery-wrap">
          <GalleryComponent></GalleryComponent>
        </div>
      </div>
    );
  }
}

export default App;
